Justin and Zach's Entry for CST-315 Assignment 2: Monitors and Semaphores

Installation Steps:
Install Linux VirtualBox
Install and configure Ubuntu virtual machine
Install G++ and GCC Packages on Ubuntu machine
Take 'monitor' and 'semaphore' file from Bitbucket repository
These programs are written in C, so save these files as a ".c" file
Use terminal commands "gcc -pthread Semaphore.c -o Semaphore", "./Semaphore" to compile and run
Use terminal commands "gcc -pthread Monitor.c -o Monitor", "./Monitor" to compile and run